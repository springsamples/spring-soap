package com.springsoap.repository;


import com.springsoap.services.bookservice.Book;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepository {

    private static final List<Book> books = new ArrayList<>();

    @PostConstruct
    public void initData() {
        Book book1 = new Book();
        book1.setName("Don Quijote");
        book1.setAuthor("Miguel de Cervantes");
        book1.setPrice("$10");

        books.add(book1);

        Book book2 = new Book();
        book2.setName("Alice's Adventures in Wonderland");
        book2.setAuthor("Lewis Carroll");
        book2.setPrice("$12");

        books.add(book2);
    }

    public Book findBook(String name) {
        Book result = null;

        for (Book book : books) {
            if (book.getName().toLowerCase().contains(name.toLowerCase())) {
                result = book;
            }
        }

        return result;
    }

    public void addBook(Book book) {
        books.add(book);
    }
}
