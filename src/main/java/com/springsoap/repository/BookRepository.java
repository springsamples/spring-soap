package com.springsoap.repository;

import com.springsoap.services.bookservice.Book;

public interface BookRepository {

    Book findBook(String name);
    void addBook(Book book);
}